import 'package:first_app/themes/custom_theme.dart';
import 'package:flutter/material.dart';

class BottomNavigation extends StatefulWidget {

  // static Color activeColor = CustomTheme.primaryColorLight;
  // static Color inActiveColor = CustomTheme.grayColor1;

  @override
  _BottomNavigationState createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  int _currentIndex = 0;

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {

    final Color activeColor =  CustomTheme.primaryColorLight;
    final Color inActiveColor =  CustomTheme.grayColor1;

    return SizedBox(
      height: 60.0,
          child: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        onTap: onTabTapped,
        currentIndex: _currentIndex,
        unselectedItemColor: inActiveColor,
        selectedItemColor: activeColor,
        selectedLabelStyle: TextStyle(fontSize: 10.0, fontWeight: FontWeight.w700),
        unselectedLabelStyle: TextStyle(fontSize: 10.0, fontWeight: FontWeight.w500),
        items: [
          BottomNavigationBarItem(
            icon: Image.asset(
                'assets/images/icons/home.png',
                width: 17.0,
                color: _currentIndex == 0 ? activeColor : inActiveColor,
              ),
            title: new Text('HOME',
              style: TextStyle(height: 2.2),
            ),
          ),

          BottomNavigationBarItem(
            icon: Image.asset(
                'assets/images/icons/classes.png',
                width: 20.0,
                color: _currentIndex == 1 ? activeColor : inActiveColor,
              ),
            title: new Text('CLASSES',
              style: TextStyle(height: 2.2),
            ),
          ),

           BottomNavigationBarItem(
            icon: Image.asset(
                'assets/images/icons/trainer.png',
                width: 19.0,
                color: _currentIndex == 2 ? activeColor : inActiveColor,
              ),
            title: new Text('TRAINERS',
              style: TextStyle(height: 2.2),
            ),
          ),

           BottomNavigationBarItem(
            icon: Image.asset(
                'assets/images/icons/feed.png',
                width: 20.0,
                color: _currentIndex == 3 ? activeColor : inActiveColor,
              ),
            title: new Text('FEED',
              style: TextStyle(height: 2.2),
            ),
          ),

           BottomNavigationBarItem(
            icon: Image.asset(
                'assets/images/icons/chat.png',
                width: 20.0,
                color: _currentIndex == 4 ? activeColor : inActiveColor,
              ),
            title: new Text('MY CHATS',
              style: TextStyle(height: 2.2),
            ),
          ),
        ],
      ),
    );
  }
}