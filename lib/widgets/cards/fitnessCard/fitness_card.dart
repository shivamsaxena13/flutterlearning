import 'package:first_app/themes/custom_theme.dart';
import 'package:flutter/material.dart';

class FitnessCard extends StatelessWidget {
  final String title;

  FitnessCard({this.title = ''});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 16.0),
      width: 190.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            height: 114.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              image: DecorationImage(
                image: NetworkImage('https://upfitness.com.hk/media/Leg-workout-2-routine-table.jpg'),
                fit: BoxFit.fill
              )
            ),
            child: null
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(3.0, 6.0, 4.0, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Text(
                        title,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.headline5,
                      ),
                    ),
                    
                    Padding(
                      padding: const EdgeInsets.only(left: 6.0),
                      child: Text(
                         '\$7.20',
                         style: Theme.of(context).textTheme.headline5.merge(
                           TextStyle(color: CustomTheme.greenColor)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
      ],),
    );
  }
}
