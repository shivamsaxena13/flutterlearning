import 'package:first_app/themes/custom_theme.dart';
import 'package:flutter/material.dart';

class ClassCard extends StatelessWidget {
  final title;
  final caption;
  final bool moreCardsShadow;

  ClassCard({this.title = '', this.caption = '', this.moreCardsShadow = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 16.0),
      width: 190.0,
      child: Stack(
        fit: StackFit.expand,
        children: [
          moreCardsShadow ? Positioned(
            bottom: 1,
            right: 18,
            left: 18,
            height: 20,
            child: Container(
              decoration: BoxDecoration(
                color: CustomTheme.grayColor4.withOpacity(0.5),
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),
          ) : Container(),

         moreCardsShadow ? Positioned(
            bottom: 8,
            right: 8,
            left: 8,
            height: 20,
            child: Container(
              decoration: BoxDecoration(
                color: CustomTheme.grayColor4,
                borderRadius: BorderRadius.circular(8.0),
              ),
            ),
          ) : Container(),
          Container(
            height: 104.0,
            margin: EdgeInsets.only(bottom: 16.0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                image: DecorationImage(
                    image: NetworkImage(
                        'https://images.unsplash.com/photo-1556817411-31ae72fa3ea0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60'),
                    fit: BoxFit.fill)),
          ),
          Positioned(
            top: 40,
            left: 0,
            right: 0,
            bottom: 16,
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    Colors.transparent,
                    const Color(0xff1A2A38).withOpacity(0.7),
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 12.0, bottom: 24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Flexible(
                  child: Text(
                    title,
                    style: Theme.of(context)
                        .textTheme
                        .headline1
                        .merge(TextStyle(color: Colors.white)),
                  ),
                ),
                caption != ''
                    ? Text(
                        caption,
                        style: Theme.of(context)
                            .textTheme
                            .subtitle1
                            .merge(TextStyle(color: Colors.white)),
                      )
                    : Container(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
