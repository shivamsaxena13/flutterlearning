import 'package:first_app/themes/custom_theme.dart';
import 'package:flutter/material.dart';

class UpcomingClassCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 290.0,
      child: Card(
        elevation: 4.0,
        shadowColor: Colors.white70,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
        ),
        margin: EdgeInsets.fromLTRB(16, 0, 0, 0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              height: 160.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(8), topRight: Radius.circular(8)),
                image: DecorationImage(
                  image: NetworkImage('https://images.unsplash.com/photo-1573384666979-2b1e160d2d08?ixlib=rb-1.2.1&auto=format&fit=crop&w=1110&q=80'),
                  fit: BoxFit.fill
                )
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  Container(
                    margin: EdgeInsets.all(6.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(3.0), color: Colors.white
                    ),
                    padding: EdgeInsets.symmetric(vertical: 0, horizontal: 4.0),
                    child: Text(
                      'UPCOMING', 
                      style: Theme.of(context).textTheme.subtitle2,
                    ),
                  ),
                ],
              )
            ),

            Padding(
              padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 14),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'Yoga Classes',
                        style: Theme.of(context).textTheme.headline5,
                      ),
                      
                      Text(
                         '\$12.50',
                         style: Theme.of(context).textTheme.headline4.merge(
                           TextStyle(color: CustomTheme.greenColor)),
                      ),
                    ],
                  ),

                  Row(
                    children: [
                      Text(
                        'by: ',
                        style: Theme.of(context).textTheme.subtitle1.merge(
                           TextStyle(color: CustomTheme.grayColor1)),
                      ),

                      Text(
                        'Calvin Smith', 
                        style: Theme.of(context).textTheme.subtitle2.merge(
                           TextStyle(color: CustomTheme.grayColor1)),
                      ),
                    ],
                  ),

                  Row(children: [
                     Flexible(
                        child: Text(
                          'Intermediate • Avg. Kcal burn: 450 - 480',
                          style: Theme.of(context).textTheme.subtitle2,
                        ),
                     ),
                  ],),

                  SizedBox(height: 14.0,),

                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                    Row(children: [
                      Icon(
                        Icons.access_time, 
                        size: 14.0, 
                        color: CustomTheme.primaryColor,
                      ),

                      SizedBox(width: 5.0,),
                      
                      Text(
                        '7:00 - 8:00 PM',
                        style: Theme.of(context).textTheme.subtitle1.merge(
                          TextStyle(color: CustomTheme.grayColor1)
                        ),
                      ),

                    ],),
                
                
                    FlatButton(
                      color: CustomTheme.primaryColorLight.withOpacity(0.15),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0)
                      ),
                      child: Text(
                        'JOIN NOW', 
                        style: Theme.of(context).textTheme.headline3.merge(
                          TextStyle(color: CustomTheme.primaryColorLight)),
                      ),
                      onPressed: (){},
                    )
                  ],
                  )
                  
                ],
              ),
            )
        ],),
      ),
    );
  }
}
