import 'package:first_app/themes/custom_theme.dart';
import 'package:first_app/widgets/ratingStars/rating_stars.dart';
import 'package:flutter/material.dart';

class TrainersCard extends StatelessWidget {
  final name;
  final type;
  final double rating;

  TrainersCard({this.name = '', this.type = '', this.rating = 0});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 120,
      margin: EdgeInsets.only(right: 10.0),
      child: Stack(
        fit: StackFit.expand,
        children: [ 

          Positioned(
            width: 120,
            left: 0.0,
            top: 30.0,
            child: Container(
              height: 112,
                decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(8.0),
                border: Border.all(width: 1,color: CustomTheme.grayColor3)
              )
            ),
          ),

          Column(
          children: [

            CircleAvatar(
              radius: 35,
              backgroundImage: NetworkImage('https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60')
            ),

            Container(
              padding: const EdgeInsets.fromLTRB(12.0, 20.0, 12.0, 3.0,),
              child: Column(
                children: [
                  Text(
                    name,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context).textTheme.headline3
                        .merge(TextStyle(color: CustomTheme.grayColor1)),
                  ),
                ],
              ),
            ),

            type != '' ? Text(
              type,
              style: Theme.of(context).textTheme.headline3.merge(TextStyle(color: CustomTheme.textColor.withOpacity((0.5)))),
            ) : Container(),
          ],),

          Positioned(
          left: 30.0,
          top: 60.0,
            child: RatingStars(stars: this.rating,),
          ),
      ],),
    );
  }
}
