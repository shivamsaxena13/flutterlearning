import 'package:flutter/material.dart';

import 'package:first_app/themes/custom_theme.dart';

class SectionTitle extends StatelessWidget {
  final String title;
  final Function rightButtonHandler;

  SectionTitle({this.title = '', this.rightButtonHandler});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 5.0),
      child: Row(
        children: [
          Expanded(
            child:
                Text(this.title, style: Theme.of(context).textTheme.bodyText2),
          ),
          GestureDetector(
            onTap: () {
              this.rightButtonHandler?.call();
            },
            child: Text(
              'VIEW ALL',
              style: Theme.of(context)
                  .textTheme
                  .headline6
                  .merge(TextStyle(color: CustomTheme.primaryColorLight)),
            ),
          ),
        ],
      ),
    );
  }
}