import 'package:flutter/material.dart';
import 'package:first_app/themes/custom_theme.dart';


AppBar header (){
  return AppBar(
    elevation: 0,
    backgroundColor: CustomTheme.primaryColor,
    leading:  IconButton(
      icon: Image.asset(
        'assets/images/icons/sidebar-toggle.png',
        width: 23.0,
      ),
      onPressed: () {},
      splashRadius: 20.0,
    ),

    actions: [
      Stack(
          children: [
          IconButton(
            icon: Image.asset(
              'assets/images/icons/bell.png',
              width: 16.0,),
            onPressed: (){},
            splashRadius: 20.0,
          ),
          Positioned(
            left: 25.0,
            top: 16.0,
            child: Container(
              decoration: BoxDecoration(
                  color: CustomTheme.themeYellow, 
                  borderRadius: BorderRadius.circular(10.0),
                  border: Border.all(width: 1.5, color: CustomTheme.primaryColor)
                ),
              width: 9.0,
              height: 9.0,
            ),
          )] 
      ),

      SizedBox(width: 8.0,),

      IconButton(
        icon: Image.asset(
          'assets/images/icons/shopping-bag.png',
          width: 20.0,),
        onPressed: (){},
        splashRadius: 20.0,
      ),
    ],
  );
}