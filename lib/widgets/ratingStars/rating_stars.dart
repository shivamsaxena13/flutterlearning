import 'package:first_app/themes/custom_theme.dart';
import 'package:flutter/material.dart';

class RatingStars extends StatelessWidget {
  final double stars;

  RatingStars({this.stars = 0});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 2, horizontal: 4),
      decoration: BoxDecoration(
        color: CustomTheme.themeYellowFade,
        borderRadius: BorderRadius.circular(3.0),
      ),
      child: Row(
        children: [
          Container(
            decoration: BoxDecoration(
              color: CustomTheme.themeYellow,
              borderRadius: BorderRadius.circular(3.0),
            ),
            margin: EdgeInsets.only(right: 4.0),
            padding: EdgeInsets.all(1.6),
            child: Image(
              image: AssetImage('assets/images/icons/star.png'),
              color: Colors.white,
              width: 12,
            ),
          ),
          Text(
            this.stars.toString(),
            style: Theme.of(context).textTheme.headline3,
          ),
          Text(
            '/5',
            style: Theme.of(context).textTheme.headline3.merge(
                TextStyle(color: CustomTheme.themeBrown.withOpacity(0.6))),
          ),
        ],
      ),
    );
  }
}
