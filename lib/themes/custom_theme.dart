import 'package:flutter/material.dart';

class CustomTheme {
  static final primaryColor = Color(0xff203399); 
  static final primaryColorLight = Color(0xff3f3efb); 
  static final greenColor = Color(0xff1ab278); 

  static final grayColor1 = Color(0xff3a4f61); 
  static final grayColor2 = Color(0xffFBFBFC); 
  static final grayColor3 = Color(0xffE8ECF3); 
  static final grayColor4 = Color(0xffCED4DB); 

  static final textColor = Color(0xff0E304E); 

  static final themeYellowFade = Color(0xffFCEBCC); 
  static final themeYellow = Color(0xffFFAB0B); 
  static final themeBrown = Color(0xff4E350E); 
}