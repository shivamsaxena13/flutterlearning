import 'package:first_app/themes/custom_theme.dart';
import 'package:flutter/material.dart';

ThemeData appTheme() {
  return ThemeData(

    // Define the default font family.
    fontFamily: 'Poppins',

    // Use this to specify the default
    // text styling for headlines, titles, bodies of text, and more.
    textTheme: TextTheme(
      headline6: TextStyle(fontSize: 12.0, fontWeight: FontWeight.w700, letterSpacing: 0.7),
      headline5: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w500),
      headline4: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w500),
      headline3: TextStyle(fontSize: 13.0, fontWeight: FontWeight.w500),
      headline2: TextStyle(fontSize: 10.0, fontWeight: FontWeight.w700),
      headline1: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300),

      subtitle1: TextStyle(fontSize: 12.0, fontWeight: FontWeight.w300, letterSpacing: 0.7, ),
      subtitle2: TextStyle(fontSize: 11.0, fontWeight: FontWeight.w500),


      bodyText1: TextStyle(fontSize: 15.0, fontWeight: FontWeight.w300),
      bodyText2: TextStyle(fontSize: 17.0, fontWeight: FontWeight.w500),
    ).apply(
      bodyColor: CustomTheme.textColor,
      displayColor: CustomTheme.textColor,
    ),
  );
}
