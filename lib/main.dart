import 'package:flutter/material.dart';
import 'package:first_app/themes/app_theme.dart';
import 'package:first_app/screens/home/home.dart';

void main() => runApp(
  MaterialApp(
    initialRoute: '/',
    theme: appTheme(),
    routes: {
      '/': (context) => Home(),
    },
));

