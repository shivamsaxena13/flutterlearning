import 'package:flutter/material.dart';
import 'package:first_app/themes/custom_theme.dart';

class UserGreeting extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 8.0),
      padding: const EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 12.0),
      color: CustomTheme.primaryColor,
      child: Row(
        children: [
          Container(
            width: 40,
            height: 40,
            margin: EdgeInsets.only(right: 8.0),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.white, width:  2.0), 
              borderRadius: BorderRadius.all(Radius.circular(50.0))),
            child: CircleAvatar(backgroundImage: NetworkImage('https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=400&q=60'),
            ),
          ),

          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Good morning', 
                style: Theme.of(context).textTheme.bodyText1.merge(
                  TextStyle(color:Colors.white)),
              ),

              Row(
                children:[
                  Text(
                    'Prateek',
                    style: Theme.of(context).textTheme.bodyText2.merge(
                    TextStyle(color:Colors.white, height: 1.1)),
                  ),
                  Icon(Icons.arrow_right, color: Colors.white,)
                ]
              )
            ],
          )
        ],
      ),
    );
    
  }
}