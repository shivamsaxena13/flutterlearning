import 'package:first_app/widgets/cards/classCard/class_card.dart';
import 'package:first_app/widgets/cards/trainersCard/trainers_card.dart';
import 'package:flutter/material.dart';

// Widgets
import 'package:first_app/widgets/cards/upcomingClassCard/upcoming_class_card.dart';
import 'package:first_app/widgets/cards/fitnessCard/fitness_card.dart';
import 'package:first_app/widgets/sectionTitle/section_title.dart';
import 'package:first_app/widgets/header/header.dart';
import 'package:first_app/widgets/bottomNavigation/bottom_navigation.dart';
import 'package:first_app/screens/home/user_greetings.dart';

// Theme
import 'package:first_app/themes/custom_theme.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      backgroundColor: CustomTheme.grayColor2,
      appBar: header(),

      body: SingleChildScrollView(
        physics: ClampingScrollPhysics(),
        padding: EdgeInsets.only(bottom: 30.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start, 
          children: [
          UserGreeting(),
          
          SectionTitle(
            title: 'Live & Upcoming Classes',
            rightButtonHandler: () {
              print('hello');
            },
          ),

          Container(
            height: 330.0,
            padding: EdgeInsets.only(bottom: 10),
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 8.0, bottom: 8.0, right: 16.0),
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                UpcomingClassCard(),
                UpcomingClassCard(),
                UpcomingClassCard(),
              ],
            ),
          ),

          SectionTitle(
            title: 'Top Fitness Plans',
            rightButtonHandler: () {
              print('hello');
            },
          ),

          Container(
            height: 170.0,
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 8.0, bottom: 8.0, right: 16.0),
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                FitnessCard(title: 'Fitness Card 1',),
                FitnessCard(title: 'Fitness Plan for a great health',),
                FitnessCard(title: 'Fitness Card 231',),
              ],
            ),
          ),

          SectionTitle(
            title: 'Recommended for you',
            rightButtonHandler: () {},
          ),

          Container(
            height: 150.0,
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 6.0, bottom: 10.0, right: 16.0),
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                ClassCard(
                  title: 'Fitness Plan 1', 
                  caption: 'Avg. Kcal burn: 450-450', 
                  moreCardsShadow: true
                ),
                ClassCard(title: 'Pilates Content & Plans', moreCardsShadow: true),
                ClassCard(
                  title: 'Yoga & Stretching', 
                  caption: 'Avg. Kcal burn: 450-450', 
                  moreCardsShadow: true
                ),
              ],
            ),
          ),

          SectionTitle(
            title: 'Trainers',
            rightButtonHandler: () {},
          ),

          Container(
            height: 180.0,
            padding: EdgeInsets.only(bottom: 14),
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.fromLTRB(16, 16.0, 6.0, 8.0),
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                TrainersCard(name: 'Laurel Wade', type: 'Yoga', rating: 3.1,),

                TrainersCard(name: 'Dan Fox', type: 'Meditation', rating: 4.8,),

                TrainersCard(name: 'Pamela Matt hew', type: 'Cardio Box', rating: 2.2,),

                TrainersCard(name: 'Laurel Wade', type: 'Yoga', rating: 1.9,),
              ],
            ),
          ),

          SectionTitle(
            title: 'Classes by Categories',
            rightButtonHandler: () {},
          ),

          Container(
            height: 160.0,
            child: ListView(
              shrinkWrap: true,
              padding: EdgeInsets.only(top: 6.0, bottom: 20.0, right: 16.0),
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                ClassCard(title: 'Power ABS'),
                ClassCard(title: 'Meditation'),
                ClassCard(title: 'Cross-Training',),
              ],
            ),
          ),

        ]),
      ),

      bottomNavigationBar: BottomNavigation()
    );
  }
}
